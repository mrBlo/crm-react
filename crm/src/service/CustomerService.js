import axios from 'axios';

const VALUE = 'customers'
const API_URL = 'http://localhost:8080/api/v1'
const CUSTOMERS_API_URL = `${API_URL}/${VALUE}` 

class CustomerService{
    //get all
    retrieveAllCustomers() {
        return axios.get(`${CUSTOMERS_API_URL}`); //Call the REST API with the GET method.
    }

       //Delete
       deleteCustomer(id) {
        //console.log('executed service')
        return axios.delete(`${CUSTOMERS_API_URL}/${id}`);
    } 

    //Get One
    retrieveCustomer( id) {
        return axios.get(`${CUSTOMERS_API_URL}/${id}`);
    }

    //Create
    createCustomer(customer) {
        return axios.post(`${CUSTOMERS_API_URL}`, customer);
    }

    //Update 
    updateCustomer(id, customer) {
        return axios.put(`${CUSTOMERS_API_URL}/${id}`, customer);
    }
    
}

export default new CustomerService();