import React from 'react';
import './App.css';
import CustomerApp from './component/CustomerApp';
//import Customers from './component/Customers'

function App() {
  return (
    <div className="container-fluid">
      <nav>
        <div className="nav-wrapper center-align">
          <a href="/" className="brand-logo">
            CRM
          </a>
        </div>
      </nav>

      <div className="row">
        <CustomerApp />
      </div>
    </div>
  );
}

export default App;
