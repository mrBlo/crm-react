import React, { Component } from 'react';
import CustomerService from '../service/CustomerService';

class CustomersList extends Component {

    constructor(props) {
        super(props)
        this.state = {
            customers: [],
            message: null
        }
        this.refreshCustomers = this.refreshCustomers.bind(this) //Any method in this react component should be bound to this. 
        this.deleteCustomerClicked = this.deleteCustomerClicked.bind(this) //binding onclick to func
        this.updateCustomerClicked = this.updateCustomerClicked.bind(this)
        this.addCustomerClicked = this.addCustomerClicked.bind(this)
    }

    //componentDidMount is called as soon as the component is mounted.
    componentDidMount() {
        this.refreshCustomers();
    }

    refreshCustomers() {
        CustomerService.retrieveAllCustomers()
            .then(
                response => {
                    console.log(response);
                    this.setState({ customers: response.data }) //update the state with response data
                }
            )
    }

    deleteCustomerClicked(id) {
        CustomerService.deleteCustomer(id)
            .then(
                response => {
                    this.setState({ message: `Customer ${id} Deleted Successfully` })
                    this.refreshCustomers()
                }
            )

    }

    updateCustomerClicked(id) {
        console.log('update ' + id)
        this.props.history.push(`/customers/${id}`)
    }

    addCustomerClicked() {
        this.props.history.push(`/customers/-1`)
    }


    render() {
        return (
            <div className="container">
                <h5>All Customers</h5>
                {this.state.message && <div className="alert alert-success">{this.state.message}</div>}
                <div className="container">
                    <table className="table highlight centered responsive-table">
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email Address</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.customers.map(
                                    customer =>
                                        <tr key={customer.id}>
                                            <td>{customer.firstName}</td>
                                            <td>{customer.lastName}</td>
                                            <td>{customer.email}</td>
                                            <td><button className="waves-effect waves-light btn"
                                                onClick={() => this.updateCustomerClicked(customer.id)}>
                                                <i className="material-icons left">edit</i>
                                            Edit</button></td>

                                            <td><button className="waves-effect waves-light red darken-1 btn"
                                                onClick={() => this.deleteCustomerClicked(customer.id)}>
                                                <i className="material-icons left">delete</i>
                                            Delete</button></td>

                                        </tr>
                                )
                            }
                        </tbody>
                    </table>
                    {/* Add Button  */}
                    <div className="row center-align">
                        <button className="waves-effect waves-light btn" onClick={this.addCustomerClicked}>
                            <i className="material-icons left">add</i>
                         Add</button>
                    </div>
                </div>
            </div>
        );
    }


}

export default CustomersList;