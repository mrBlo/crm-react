import React, {Component} from 'react';
import CustomersList from './CustomersList';
import Customer from './Customer';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

class CustomerApp extends Component {
    render() {
        return (<Router>
            <>
                <h5>Customer Application</h5>
                
                <Switch>
                    <Route path="/" exact component={CustomersList} />
                    <Route path="/customers" exact component={CustomersList} />
                    <Route path="/customers/:id" component={Customer} />
                </Switch>
            </>
        </Router>
        )
    }
}

export default CustomerApp