import React, { Component } from 'react';

class AddCustomer extends Component {

submitCustomer(event){
    event.preventDefault();
    let customer= {
        firstName: this.refs.firstName.value,
        lastName: this.refs.lastName.value,
        email: this.refs.email.value
    }

    //do post here
    fetch("http://localhost:8080/api/v1/customers/",{
    method: "POST",
    headers: {
        "content-type": "application/json",
    },
    body: JSON.stringify(customer) , //post body
    })
    .then(response => response.json());
            //refresh window afterwards
            window.location.reload();
}


    render() {
        return (
            <div className="row">
                <form className="col s12" onSubmit={this.submitCustomer.bind(this)}>
                    <div className="row">
                        <div className="input-field col s6">
                                <input placeholder="Placeholder" ref="firstName" type="text" className="validate"/>
                                <label htmlFor="firstName">First Name</label>
                        </div>
                            <div className="input-field col s6">
                                    <input ref="lastName" type="text" className="validate"/>
                                    <label htmlFor="last_name">Last Name</label>
                              </div>
                            </div>
                            
                                    <div className="row">
                                        <div className="input-field col s12">
                                                <input ref="email" type="email" className="validate"/>
                                                <label htmlFor="email">Email</label>
                                     </div>
                                    </div>
                                        <div className="row">
                                        <button className= "waves-effect waves-light btn" type="submit" name="action">Submit</button>  
                                        </div>
                 </form>
                                    </div>
                                    );
                                }
                            }
                            
                            
                            
export default AddCustomer;