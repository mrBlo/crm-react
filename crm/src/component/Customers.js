import React, { Component } from 'react';
import SingleCustomer from './SingleCustomer';
import AddCustomer from './AddCustomer';

class Customers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            customers: []
        };
    }

    //do a componentDidMount here to fetch data before rendering
    componentDidMount() {
        fetch("http://localhost:8080/api/v1/customers/")
            .then(response => response.json())
            .then(data => this.setState({ customers: data }))
    }


    render() {
        return (
            <div className="container">
                <div className="row">
                    <AddCustomer />
                </div>

                <div className="row">
                    {this.state.customers.map((item) => (
                        <SingleCustomer key={item.id} item={item} />
                    )
                    )}
                </div>

            </div>
        );
    }
}

export default Customers;