import React, { Component } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik';
import CustomerService from '../service/CustomerService';
import Alert from 'react-bootstrap/Alert';

class Customer extends Component {

  constructor(props) {
    super(props)

    this.state = { //initializing state in the constructor
      id: this.props.match.params.id,  //getting id from the URL param
      firstName: '',
      lastName: '',
      email: ''
    }
    this.onSubmit = this.onSubmit.bind(this) //bind the onSubmit method to onSubmit in the Form
    this.validate = this.validate.bind(this) //binding the validate methods
  }

  componentDidMount() {
    console.log(this.state.id)
    // eslint-disable-next-line
    if (this.state.id == -1) {
      return
    }
    CustomerService.retrieveCustomer(this.state.id)
      .then(response => this.setState({   //updating the state with response data
        firstName: response.data.firstName,
        lastName: response.data.lastName,
        email: response.data.email
      }))
  }
  //onSubmit method
     onSubmit(values) {
       // let username = INSTRUCTOR
    
        let customer = {
            id: this.state.id,
            firstName: values.firstName,
            lastName: values.lastName,
            email: values.email
        }
        // eslint-disable-next-line
        if (this.state.id == -1) {
          CustomerService.createCustomer(customer)
                .then(() => this.props.history.push('/customers'))
        } else {
          CustomerService.updateCustomer(this.state.id, customer)
                .then(() => this.props.history.push('/customers'))
        }

        console.log(values);
    }

    //My Render ID Element
  //   renderIdElement(){
  //     if(this.state!== -1)
  //     return( 
  //     <fieldset className="form-group">
  //     <label>Id</label>
  //     <Field className="form-control" type="text" name="id" disabled />
  //   </fieldset>
  //   );
  //  return null;
  //   }

  //Form validate method
  validate(values) {
    let errors = {}
    //first name check
    if (!values.firstName) {
      errors.firstName = 'Enter a First Name'
    } else if (values.firstName.length < 3) {
      errors.firstName = 'Enter at least 3 Characters in First Name'
    }
    //last name check
    if (!values.lastName) {
      errors.lastName = 'Enter a Last Name'
    } else if (values.lastName.length < 5) {
      errors.lastName = 'Enter at least 5 Characters in Last Name'
    }
    //email check
    if (!values.email) {
      errors.email = 'Enter an Email Address'
    } else if (values.email.length < 5) {
      errors.email = 'Enter at least 5 Characters in Email'
    }

    return errors
  }

  render() {
    let { firstName, lastName, email, id } = this.state //destructing state to variables
    return (
      <div>
        <h5>Customer</h5>
        <div className="container">
          {/* Initiating Formik with the values loaded from state */}
          {/* enableReinitialize={true} is needed to ensure that we can reload the form for existing customer */}
          <Formik
            initialValues={{id, firstName, lastName, email }}
            onSubmit={this.onSubmit}
            validateOnChange={false}
            validateOnBlur={false}
            validate={this.validate}
            enableReinitialize={true} >
            {
              (props) => (
                <Form>
      
                  <ErrorMessage name="firstName" component="div"
                    className="alert alert-warning" />
                  <ErrorMessage name="lastName" component="div"
                    className="alert alert-warning" />
                  <ErrorMessage name="email" component="div"
                    className="alert alert-warning" />

              
                  <fieldset className="form-group">
                    <label>First Name</label>
                    <Field className="form-control" type="text" name="firstName" />
                  </fieldset>
                  <fieldset className="form-group">
                    <label>Last Name</label>
                    <Field className="form-control" type="text" name="lastName" />
                  </fieldset>
                  <fieldset className="form-group">
                    <label>Email</label>
                    <Field className="form-control" type="text" name="email" />
                  </fieldset>

                  <div className="center-align">
                    <button className=" waves-effect waves-light btn pulse" type="submit">
                      <i className="material-icons left">save</i>
                                Save</button>
                  </div>

                </Form>
              )
            }


          </Formik>

        </div>
      </div>
    )
  }

}

export default Customer;